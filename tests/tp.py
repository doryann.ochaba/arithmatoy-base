# Aucun n'import ne doit être fait dans ce fichier


def nombre_entier(n: int) -> str:
    return "0" if n == 0 else "S" + nombre_entier(n - 1)

def S(n: str) -> str:
    if n == "0":
        return "S0"
    result = "S" 
    result += n
    return result

def addition(a: str, b: str) -> str:
    if a == "0":
        return b
    elif b == "0":
        return a
    else:
        return S(addition(a[:-1], b[:-1]))

def multiplication(a: str, b: str) -> str:
        if a == "0" or b == "0":
        return "0"

    int_a = 0;
    int_b = 0

    for c in a:
        if c == "S":
            int_a += 1

    for c in b:
        if c == "S":
            int_b += 1

    return nombre_entier(int_a * int_b)


def facto_ite(n: int) -> int:
    if n < 0:
        raise ValueError("Negative integer.")
    result = 1
    for i in range(1, n+1):
        result *= i
    return result

def facto_rec(n: int) -> int:
    if n < 0:
        raise ValueError("Negative integer.")
    return 1 if n == 0 else n * facto_rec(n-1)

def fibo_ite(n):
    if n == 0:
        return 0
    elif n == 1:
        return 1
    else:
        a, b = 0, 1
        for _ in range(n - 1):
            a, b = b, a + b
        return b

def fibo_ite(n):
    if n == 0:
        return 0
    elif n == 1:
        return 1
    else:
        a, b = 0, 1
        for _ in range(n - 1):
            a, b = b, a + b
        return b

def golden_phi(n: int) -> int:
    pass

def sqrt5(n):
    pass

def pow(a: float, n: int) -> float:
    a, b = 0, 1
    for i in range(n):
        a, b = b, a+b
    return a

def golden_phi(n: int) -> float:
    pass

def sqrt5(n):
    pass

def pow(x: float, n: int) -> float:
    if n == 0:
        return 1
    elif n % 2 == 0:
        return pow(x * x, n // 2)
    else:
        return x * pow(x, n-1)